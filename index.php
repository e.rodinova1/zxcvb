<?php
header('Content-Type: text/html; charset=UTF-8');

$skills_labels = [
  'immortality' => 'Immortality',
  'idclip' => 'Passing Through Walls',
  'fly' => 'Fly'
];

if ($_SERVER['REQUEST_METHOD'] == 'GET') {
  if (!empty($_GET['save'])) {
    print('Data was successfully send.</br>');
  }
  include('form.php');
  exit();
}


$errors = false;


if (empty($_POST['name'])) {
  print('Fill the "Name".</br>');
  $errors = true;
} else if (!preg_match('/^[a-zA-Z]+$/u', $_POST['name'])) {
  print('Invalid symbols in the "Name".</br>');
  $errors = true;
}

if (empty($_POST['email'])) {
  print('Fill the "Email".</br>');
  $errors = true;
} else if (!filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)) {
  print('Uncorrect "Email".</br>');
  $errors = true;
}

if (empty($_POST['birthday'])) {
  print('Pick your birthday.</br>');
  $errors = true;
} else if (!(is_numeric($_POST['birthday']) && intval($_POST['birthday'] >= 1900 && intval($_POST['birthday']) <= 2020))) {
  print('Pick a correct birthday.<br/>');
  $errors = true;
}

if (empty($_POST['sex'])) {
  print('Pick your sex.</br>');
  $errors = true;
} else if (!($_POST['sex'] === 'm' || $_POST['sex'] === 'f')) {
  print('Pick a correct sex.<br/>');
  $errors = true;
}

if (empty($_POST['limbs'])) {
  print('Pick num of your limbs.</br>');
  $errors = true;
} else if (!(is_numeric($_POST['limbs']) && intval($_POST['limbs']) >= 0 && intval($_POST['limbs']) <= 4)) {
  print('Pick a correct num of your limbs.<br/>');
  $errors = true;
}

$skill_data = array_keys($skills_labels);
if (empty($_POST['skills'])) {
  print('Pick skill(s).</br>');
  $errors = true;
} else {
  foreach ($_POST['skills'] as $skill) {
    if (!in_array($skill, $skill_data)) {
      print('Uncorrect skill.<br/>');
      $errors = TRUE;
    }
  }
}
$skill_insert = [];
foreach ($skill_data as $skill) {
  $skill_insert[$skill] = in_array($skill, $_POST['skills']) ? 1 : 0;
}

if (strlen($_POST['biography']) < 200) {
  print('Biography have to include minimum 200 symbols.</br>');
  $errors = true;
}

if (empty($_POST['contract_accept']) || $_POST['contract_accept'] != true) {
  print('You must to accept the contract.');
  $errors = true;
}

if ($errors) {
  exit();
}

// connect to BD
$user = 'u20487';
$pass = '4400740';
try {
  $db = new PDO(
    'mysql:host=localhost;dbname=u20487',
    $user,
    $pass,
    array(PDO::ATTR_PERSISTENT => true)
  );
} catch (PDOException $e) {
  exit($e->getMessage());
}


try {
  $stmt = $db->prepare("INSERT INTO user SET name = ?, email = ?, birthday = ?, sex = ?, limbs = ?,  skill_immortality = ?, skill_idclip = ?, skill_fly = ?, biography = ?, contract_accept = ?");
  $stmt->execute([$_POST['name'], $_POST['email'], intval($_POST['birthday']), $_POST['sex'], intval($_POST['limbs']), $skill_insert['immortality'], $skill_insert['idclip'], $skill_insert['fly'], $_POST['biography'], intval($_POST['contract_accept'])]);
} catch (PDOException $e) {
  exit($e->getMessage());
}

header('Location: ?save=1');
